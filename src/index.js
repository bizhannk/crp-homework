// style
import './styles/style.css';
// code
import { render, SectionCreator } from './join-us-section.js';
import { renderCommunity } from './community';

SectionCreator('standart');
const url = 'http://localhost:3000/community';

window.addEventListener('load', () => {
  render();
  renderCommunity(url);
});
