const template = document.createElement('template');
template.innerHTML = `
<style>
h2 {
  font-family: 'Oswald', sans-serif;
  font-size: 3rem;
  font-weight: 700;
  margin-top: 0;
}
</style>

<div>
    <div> <slot name="logo"></slot><div>
    <h2><slot name="title"></slot></h2>
    <p><slot name="subheading"></slot></p>
    <div> <slot name="content"></slot><div>
    <p><slot name="subheadingDown"></slot></p>
</div>
`;

export class WebSection extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

window.customElements.define('website-section', WebSection);
