import { validate } from './email-validator.js';
import { subscribe, unsubscribe } from './api.js';

const divEl = document.querySelector('#new-section');
const sectionEl = document.createElement('section');

sectionEl.classList = 'join-us container';

export const SectionCreator = (type = 'standart') => {
  let title = 'Join Our Program';
  let button = 'Subscribe';
  if (type === 'advanced') {
    title = 'Join Our Advanced Program';
    button = 'Subscribe to Advanced Program ';
  }
  return (sectionEl.innerHTML = `
    <h2>${title}</h2>
    <p class="subheading">Sed do eiusmod tempor incididunt
    ut labore et dolore magna aliqua.</p>
    <form>
      <input type="email" id="email" placeholder="Email"/>
      <button class="btn" id="btn" type="submit">${button} </button>
    </form>
  `);
};

export const render = () => {
  divEl.append(sectionEl);
  // get elements rendered from DOM
  const form = document.querySelector('form');
  const email = document.querySelector('#email');
  const btn = document.querySelector('#btn');

  // get localstorage
  const emailLs = localStorage.getItem('email');
  let isSubcribed = false;

  // set input field and button text
  if (emailLs) {
    email.value = emailLs;
    btn.textContent = 'Unsubscribe';
    isSubcribed = !isSubcribed;
  } else {
    email.value = '';
    btn.textContent = 'Subcribe';
  }

  // set localstorage on input event
  email.addEventListener('input', (e) => {
    localStorage.setItem('email', email.value);
  });

  form.addEventListener('submit', async (e) => {
    e.preventDefault();
    // remove item from localstorage, switch button's text and clear input field
    if (isSubcribed) {
      disableBtn(btn);
      await unsubscribe(email.value);
      localStorage.removeItem('email');
      btn.textContent = 'Subcribe';
      email.value = '';
      enableBtn(btn);
      isSubcribed = !isSubcribed;
    } else {
      const isValid = validate(email.value);
      if (isValid) {
        disableBtn(btn);
        await subscribe(email.value);
        btn.textContent = 'Unsubscribe';
        enableBtn(btn);
        isSubcribed = !isSubcribed;
      }
    }
  });
};

function disableBtn(btn) {
  btn.disabled = true;
  btn.style.opacity = 0.5;
}

function enableBtn(btn) {
  btn.disabled = false;
  btn.style.opacity = 1;
}
